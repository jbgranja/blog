---
title: "502 Error in LEMP Server"
date: 2018-12-27T20:46:49-06:00
tags: ["nasa"]
---

When I was developing a webpage in the Checkout Process, I got the following Error



{{< figureCupper
img="image1.png" 
command="Resize" 
options="700x" >}}


I tried to see if it was a PHP issue, but everything was ok, so I decided see error logs in Nginx too


 
{{< cmd >}}
cd /var/log/nginx
nano example.journaldev.com-error.log
{{</ cmd >}}
 
 
And I found the following issue:



{{< figureCupper
img="image2.png" 
command="Resize" 
options="1000x" >}}
    
    03/07 18:30:38 [error] 11122#11122: *8 upstream sent too big header while reading response header from upstream, client: 127.0.0.1, server: delejos2010.com, request: "POST /ecuador/carro_de_compras HTTP/1.1", u>
    


The solution for this kind of problems is to augment buffer size in the nginx config file “sites-avaliable”


    
    fastcgi_buffers         16  16k;
    fastcgi_buffer_size         32k;
    

So our file will have the following structure

    server {
    listen         80;
    listen         [::]:80;
    server_name    delejos2010.com www.delejos2010.com;
    root           /var/www/delejos;
    index          index index.php index.html index.htm;
w
    location / {
      rewrite ^/(.*)$  /index.php last;
      # try_files $uri $uri/ =404;
    }

    location ~* \.php$ {
      fastcgi_pass unix:/run/php/php5.6-fpm.sock;
      include         fastcgi_params;
      fastcgi_param   SCRIPT_FILENAME $request_filename;

      fastcgi_buffers         16  16k;
      fastcgi_buffer_size         32k;
    }
    
    error_log /var/log/nginx/example.journaldev.com-error.log;
    }





