---
title: "Dynamic Basepath Country URL in Wordpress/Woocommerce"
date: 2020-08-04T21:05:05-05:00
tags: [Wordpress,URL ReWriting,Nginx]
---
## Installig Nginx
{{< cmd >}}
sudo apt update
sudo apt install nginx
{{</ cmd >}}

Try to enter to localhost domain, http://localhost

and you will see the following page

make sure you have disabled Apache, or you will encounter issues

# Uninstalling Apache

## Installing Mysql
MYSQL 5.7

{{< cmd >}}
wget https://dev.mysql.com/get/mysql-apt-config_0.8.24-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.24-1_all.deb
sudo apt update
sudo apt-cache policy mysql-server
sudo apt install -f mysql-client=5.7* mysql-community-server=5.7* mysql-server=5.7*
sudo mysql_secure_installation
mysql -u root -p

{{</ cmd >}}


MYSQL 8


{{< cmd >}}
wget https://dev.mysql.com/get/mysql-apt-config_0.8.24-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.24-1_all.deb
sudo apt update
sudo apt install -f mysql-client=8.0* mysql-community-server=8.0* mysql-server=8.0*
mysql –V
mysqladmin -u root -p version
{{</ cmd >}}



## Installing PHP
PHP 5.6

{{< cmd >}}

sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt upgrade
sudo apt install -y php5.6
php -v
{{</ cmd >}}


Install required Modules for Website Developing
{{< cmd >}}
sudo apt install php5.6-gd php5.6-mysql php5.6-imap php5.6-curl php5.6-intl php5.6-pspell php5.6-recode php5.6-sqlite3 php5.6-tidy php5.6-xmlrpc php5.6-xsl php5.6-zip php5.6-mbstring php5.6-soap php5.6-opcache libicu65 php5.6-common php5.6-json php5.6-readline php5.6-xml
{{</ cmd >}} 



PHP 8
{{< cmd >}}
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install -y php8.1
php -v
{{</ cmd >}}

Install required Modules for Website Developing
{{< cmd >}}
apt install php8.1-gd php8.1-mysql php8.1-imap php8.1-curl php8.1-intl php8.1-pspell php8.1-sqlite3 php8.1-tidy php8.1-xsl php8.1-zip php8.1-mbstring php8.1-soap php8.1-opcache libonig5 php8.1-common php8.1-readline php8.1-xml
{{</ cmd >}} 


## Configuring NginxServer 

Check Nginx is running properly


{{< cmd >}}
systemctl status nginx

{{</ cmd >}} 

First we want to create a folder for the domain files will be stored

{{< cmd >}}

sudo mkdir -p /var/www/your_domain/html
{{</ cmd >}} 

Change owner propeties for the filder


{{< cmd >}}
sudo chown -R $USER:$USER /var/www/your_domain/html
{{</ cmd >}} 


Then we have to setup permission for nginx cre create and delete files in the folder destination


{{< cmd >}}
sudo chmod -R 755 /var/www/your_domain

{{</ cmd >}} 

For Testing Purposes
{{< cmd >}}
sudo nano /var/www/your_domain/html/index.html

{{</ cmd >}} 

```html
<html>
    <head>
        <title>Welcome to your_domain!</title>
    </head>
    <body>
        <h1>Success!  The your_domain server block is working!</h1>
    </body>
</html>
```

We are goinf to create the nginx fonig file for the webpage:

{{< cmd >}}

sudo nano /etc/nginx/sites-available/your_domain
{{</ cmd >}} 

and paste the followwing code modifying with your website domain:


    server {
        listen 80;
        listen [::]:80;

        root /var/www/your_domain/html;
        index index.html index.htm index.nginx-debian.html;

        server_name your_domain www.your_domain;

        location / {
                try_files $uri $uri/ =404;
        }
    }



Notice that we’ve updated the root configuration to our new directory, and the server_name to our domain name.

Next, let’s enable the file by creating a link from it to the sites-enabled directory, which Nginx reads from during startup:

{{< cmd >}}
sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/
{{</ cmd >}} 




{{< cmd >}}
sudo nginx -t
sudo systemctl restart nginx
{{</ cmd >}} 

Nginx should now be serving your domain name. You can test this by navigating to http://your_domain, where you should see something like this:

