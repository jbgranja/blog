---
title: "installing i3wm on Ubuntu 20.04"
date: 2018-12-27T20:46:49-06:00
tags: ["i3,ubuntu20.04"]
---

{{< cmd >}}
sudo apt install i3

{{</ cmd >}}


You have to log out and then log in in options choose I3 Desktop Environment:

once you're in check the option WIN+ alt can have some issues because another keywords could be reserved in other apps, WIN key is a better option

I3, comes with default configuration for some basic but for major shortcuts you have to configure by yourself i put some of the mos used shortcuts to up and down volume and increase brightness of the screen and decrease:

Check if config file is created in this direction: “.config/i3/config” otherwise create it:


{{< cmd >}}
nano .config/i3/config
{{</ cmd >}}
 


Volume Control


Switch Betweeen WIndows (like WIN+ Tab)

    #switch programs
    bindsym $mod+Tab exec "i3-msg workspace back_and_forth"

Touchpad Enabled

    #Activate Tapping Touchpad
    exec xinput set-prop "DELL086F:00 06CB:7E92 Touchpad" "libinput Tapping Enabled" 1


Screen brightness Control




install xbacklight helper:

{{< cmd >}}
sudo apt install i3
{{</ cmd >}}

    # Sreen brightness controls
    bindsym XF86MonBrightnessUp exec xbacklight -inc 3
    bindsym XF86MonBrightnessDown exec xbacklight -dec 3 # decrease screen brightness

Screnshot Shortcut

    #Screenshot command
    bindsym Control+Print exec gnome-screenshot -i











